/**
 * 專案名稱： gof-adapter
 * 檔案說明： 5V 電源適配器
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Adapter, Power } from './../../core';

/**
 * 5V 電源適配器
 */
export class Adapter5v extends Adapter {
  /**
   * @param power 電源
   */
  constructor(protected power: Power) {
    super(power);
  }

  /**
   * 轉接電源
   *
   * @method public
   * @return 回傳特定電壓電源
   */
  public supply(): number {
    const voltage = this.power.supply();
    if (voltage === 220) {
      console.log('convert volatage 220V to 5V');
      return voltage / 44;
    } else if (voltage === 110) {
      console.log('convert volatage 110V to 5V');
      return voltage / 22;
    } else {
      throw new Error(`Can't process voltahe: ${voltage}V`);
    }
  }
}
