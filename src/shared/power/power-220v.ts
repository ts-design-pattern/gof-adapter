/**
 * 專案名稱： gof-adapter
 * 檔案說明： 220V 電源
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Power } from './../../core';

/**
 * 220V 電源
 */
export class Power220v implements Power {
  /**
   * 供應電源
   *
   * @method public
   * @return 回傳特定電壓電源
   */
  public supply(): number {
    return 220;
  }
}
