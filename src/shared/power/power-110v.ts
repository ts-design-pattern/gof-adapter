/**
 * 專案名稱： gof-adapter
 * 部門代號： ML8100
 * 檔案說明： 110V 電源
 * @CREATE Saturday, 9th October 2021 3:00:28 pm
 * @author Steve Y Lin
 * @contact Steve_Y_Lin@wistron.com #1342
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Power } from './../../core';

/**
 * 110V 電源
 */
export class Power110v implements Power {
  /**
   * 供應電源
   *
   * @method public
   * @return 回傳特定電壓電源
   */
  public supply(): number {
    return 110;
  }
}
