/**
 * 專案名稱： gof-adapter
 * 檔案說明： 適配器模式範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Adapter20v, Adapter5v, Power110v, Power220v } from './shared';

const power220v = new Power220v();
const power110v = new Power110v();

const adapter20v = new Adapter20v(power220v);
adapter20v.supply();

const adapter5v = new Adapter5v(power110v);
adapter5v.supply();
