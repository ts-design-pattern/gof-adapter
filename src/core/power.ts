/**
 * 專案名稱： gof-adapter
 * 檔案說明： 抽象電源
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 抽象電源
 */
export interface Power {
  /**
   * 供應電源
   *
   * @method public
   * @return 回傳特定電壓電源
   */
  supply(): number;
}
