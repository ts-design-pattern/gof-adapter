/**
 * 專案名稱： gof-adapter
 * 檔案說明： 抽象適配器
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Power } from './power';

/**
 * 抽象適配器
 */
export abstract class Adapter {
  /**
   * @param power 電源
   */
  constructor(protected power: Power) {}

  /**
   * 轉接電源
   *
   * @method public
   * @return 回傳特定電壓電源
   */
  public abstract supply(): number;
}
