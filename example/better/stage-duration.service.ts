/**
 * 專案名稱： gof-adapter
 * 檔案說明： 機台過站時間計算服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { Consumer } from './core';
import { Duration, DurationService } from './shared';

/**
 * 機台過站時間計算服務
 */
export class StageDurationService {
  /**
   * 過站時間計算服務
   */
  private _durationService = new DurationService();
  /**
   * 訂閱項目
   */
  private _sub$?: Observable<Duration | null>;

  /**
   * @param _consumer 資料消費者
   */
  constructor(private _consumer: Consumer) {}

  /**
   * 印出過站時間
   *
   * @method private
   */
  private print(duration: Duration): void {
    console.log(
      `${duration.from} to ${duration.to} duration: ${duration.duration}`,
    );
  }

  /**
   * 執行過站時間計算
   *
   * @method public
   */
  public execute(): void {
    this._sub$ = this._consumer.consume().pipe(
      // 計算過站時間
      map(output => this._durationService.add(output).calculate()),
      // 排除 NULL 數據
      filter(duration => duration !== null),
      // 印出過站時間
      tap(duration => this.print(duration as Duration)),
    );

    this._sub$.subscribe();
  }
}
