/**
 * 專案名稱： gof-adapter
 * 檔案說明： 抽象消費者
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable } from 'rxjs';

/**
 * 抽象消費者
 */
export interface Consumer<D = any> {
  /**
   * 消費資料
   *
   * @method public
   * @return 取得要消費的資料
   */
  consume(): Observable<D>;
}
