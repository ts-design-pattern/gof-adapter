/**
 * 專案名稱： gof-adapter
 * 檔案說明： MQTT 資料消費者轉接器
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Observable } from 'rxjs';
import { ConsumerAdapter } from './../../core';
import { Mqtt } from './../mqtt';

/**
 * MQTT 資料消費者轉接器
 */
export class MqttConsumerAdapter<D = any> extends ConsumerAdapter<Mqtt, D> {
  /**
   * @param consumer 資料消費者
   * @param topic    要訂閱的主題
   */
  constructor(protected consumer: Mqtt, protected topic: string) {
    super(consumer);
  }

  /**
   * 消費資料
   *
   * @method public
   * @return 取得要消費的資料
   */
  public consume(): Observable<D> {
    return new Observable(sub => {
      this.consumer.on('connect', () => this.consumer.subscribe(this.topic));
      this.consumer.on('message', (_, msg) => sub.next(JSON.parse(msg)));
    });
  }
}
