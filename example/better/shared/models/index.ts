/**
 * 專案名稱： gof-adapter
 * 檔案說明： 資料模型匯出點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

export * from './duration.model';
export * from './output.model';
