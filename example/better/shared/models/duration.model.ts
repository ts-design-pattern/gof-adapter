/**
 * 專案名稱： gof-adapter
 * 檔案說明： 機台過站時間資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */
/* eslint-disable camelcase */

/**
 * 機台過站時間資料模型
 */
export interface Duration {
  /**
   * 機台當站時間
   */
  evt_dt: number;
  /**
   * Site
   */
  site: string;
  /**
   * 廠別代碼
   */
  plant: string;
  /**
   * 線別
   */
  line: string;
  /**
   * 上一站別
   */
  from: string;
  /**
   * 當前站別
   */
  to: string;
  /**
   * 機台序號
   */
  usn: string;
  /**
   * 過站時間，單位毫秒
   */
  duration: number;
}
