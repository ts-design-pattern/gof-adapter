/**
 * 專案名稱： gof-adapter
 * 檔案說明： 機台資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */
/* eslint-disable camelcase */

/**
 * 機台資料模型
 */
export interface Output {
  /**
   * 機台當站時間
   */
  evt_dt: number;
  /**
   * Site
   */
  site: string;
  /**
   * 廠別代碼
   */
  plant: string;
  /**
   * 線別
   */
  line: string;
  /**
   * 站別
   */
  stage: string;
  /**
   * 機台序號
   */
  usn: string;
}
