/**
 * 專案名稱： gof-adapter
 * 檔案說明： 機台過站時間計算功能匯出點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

export * from './duration.builder';
export * from './duration.service';
