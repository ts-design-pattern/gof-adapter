/**
 * 專案名稱： gof-adapter
 * 檔案說明： 機台過站時間建構者
 * -----------------------------------------------------------------------------
 * @NOTE
 */
/* eslint-disable camelcase */

import { Duration, Output } from '../models';

/**
 * 機台過站時間建構者
 */
export class DurationBuilder implements Duration {
  /**
   * 機台當站時間
   */
  public evt_dt = new Date().getTime();
  /**
   * Site
   */
  public site = '';
  /**
   * 廠別代碼
   */
  public plant = '';
  /**
   * 線別
   */
  public line = '';
  /**
   * 上一站別
   */
  public from = '';
  /**
   * 當前站別
   */
  public to = '';
  /**
   * 機台序號
   */
  public usn = '';
  /**
   * 過站時間，單位毫秒
   */
  public duration = 0;

  /**
   * @param last   上一次的機台數據
   * @param latest 當前機台數據
   */
  constructor(last: Output, latest: Output) {
    this.site = latest.site;
    this.plant = latest.plant;
    this.line = latest.line;
    this.from = last.stage;
    this.to = latest.stage;
    this.usn = latest.usn;
    this.duration = latest.evt_dt - last.evt_dt;
  }
}
