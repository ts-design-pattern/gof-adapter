/**
 * 專案名稱： gof-adapter
 * 檔案說明： 較佳的開發模式程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import {
  ConsumerGroup,
  KafkaConsumerAdapter,
  Mqtt,
  MqttConsumerAdapter,
  Output,
} from './shared';
import { StageDurationService } from './stage-duration.service';

// 使用 MQTT 作為資料來源計算過站時間
setTimeout(() => {
  console.log('use mqtt to fetch data');
  const mqttClient = new Mqtt().connect('mqtt://localhost:1883/');
  const mqttTopic = 'wks/cim/m1/output/#';
  const mqttAdapter = new MqttConsumerAdapter<Output>(mqttClient, mqttTopic);
  const mqttDurationService = new StageDurationService(mqttAdapter);
  mqttDurationService.execute();
}, 500);

// 使用 Kafka 作為資料來源計算過站時間
setTimeout(() => {
  console.log('use kafka to fetch data');
  const kafkaHost = 'localhost:1883';
  const groupId = 'test.group.id';
  const kafkaTopic = 'wks.cim.m1.output';
  const consumerGroup = new ConsumerGroup({ kafkaHost, groupId }, kafkaTopic);
  const kafkaAdapter = new KafkaConsumerAdapter<Output>(consumerGroup);
  const kafkaDurationService = new StageDurationService(kafkaAdapter);
  kafkaDurationService.execute();
}, 6500);
