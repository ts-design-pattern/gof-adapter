/**
 * 專案名稱： gof-adapter
 * 檔案說明： 不好的範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { KafkaDurartionService } from './kafka-durartion.service';
import { MqttDurartionService } from './mqtt-durartion.service';
import { ConsumerGroup, Mqtt } from './shared';

// 使用 MQTT 作為資料來源計算過站時間
setTimeout(() => {
  const mqttClient = new Mqtt().connect('mqtt://localhost:1883/');
  mqttClient.on('connect', () => mqttClient.subscribe('wks/cim/m1/output/#'));
  const mqttDuration = new MqttDurartionService(mqttClient);
  mqttDuration.execute();
}, 500);

// 使用 Kafka 作為資料來源計算過站時間
setTimeout(() => {
  const kafkaHost = 'localhost:1883';
  const groupId = 'test.group.id';
  const kafkaTopic = 'wks.cim.m1.output';
  const consumerGroup = new ConsumerGroup({ kafkaHost, groupId }, kafkaTopic);
  const kafkaDuration = new KafkaDurartionService(consumerGroup);
  kafkaDuration.execute();
}, 6500);
