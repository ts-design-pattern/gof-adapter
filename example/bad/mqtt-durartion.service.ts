/**
 * 專案名稱： gof-adapter
 * 檔案說明： 使用 MQTT 過站時間計算服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { DurationService, Mqtt } from './shared';

/**
 * 使用 MQTT 過站時間計算服務
 */
export class MqttDurartionService {
  /**
   * 過站時間計算服務
   */
  private _durationService = new DurationService();

  /**
   * @param _mqtt MQTT
   */
  constructor(private _mqtt: Mqtt) {}

  /**
   * 執行過站時間計算
   *
   * @method public
   */
  public execute(): void {
    this._mqtt.on('message', (topic, message) => {
      message = JSON.parse(message);
      const duration = this._durationService.add(message).calculate();
      if (duration !== null) {
        console.log(
          `${duration.from} to ${duration.to} duration: ${duration.duration} by mqtt`,
        );
      }
    });
  }
}
