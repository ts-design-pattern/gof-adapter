/**
 * 專案名稱： gof-adapter
 * 檔案說明： Mock MQTT
 * -----------------------------------------------------------------------------
 * @NOTE
 */
/* eslint-disable no-undef */

import * as OUTPUT from './../../../../mock/data/output.json';

/**
 * Mock MQTT
 */
export class Mqtt {
  /**
   * 假資料時間戳
   */
  private _timer?: NodeJS.Timeout;
  /**
   * 訂閱主題
   */
  private _topic = '';

  /**
   * 連線
   *
   * @method public
   * @param host MQTT Host
   */
  public connect(host: string): this {
    console.log(`connect to mqtt ${host}`);
    return this;
  }

  /**
   * 訂閱特定主題
   *
   * @method public
   * @param topic 要訂閱的主題
   */
  public subscribe(topic: string): void {
    this._topic = topic;
  }

  /**
   * MQTT 事件
   *
   * @method public
   * @param type     事件類型
   * @param callback 事件對應回乎函數
   */
  public on(
    type: 'connect' | 'message',
    callback?: (...args: any) => void,
  ): void {
    if (type === 'connect' && callback) {
      callback();
    } else if (type === 'message' && callback) {
      const outputs = OUTPUT.entries();
      this._timer = setInterval(() => {
        const output = outputs.next();
        if (!output.done) {
          callback(
            this._topic.replace(/#/g, ''),
            JSON.stringify(output.value[1]),
          );
        } else {
          if (this._timer) {
            clearInterval(this._timer);
          }
        }
      }, 1000);
    }
  }
}
