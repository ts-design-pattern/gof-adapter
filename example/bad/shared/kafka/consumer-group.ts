/**
 * 專案名稱： gof-adapter
 * 檔案說明： Mock Kafka Consumer Group
 * -----------------------------------------------------------------------------
 * @NOTE
 */
/* eslint-disable no-undef */
/* eslint-disable node/no-callback-literal */

import * as OUTPUT from './../../../../mock/data/output.json';
import { CONSUMERT_OPTS } from './consumer.option';

/**
 * Mock Kafka Consumer Group
 */
export class ConsumerGroup {
  /**
   * 假資料時間戳
   */
  private _timer?: NodeJS.Timeout;

  /**
   * @param _options Consumer 配置
   * @param _topic   要訂閱的主題
   */
  constructor(private _options: any = CONSUMERT_OPTS, private _topic: string) {
    console.log(`connect to kafka ${_options.kafkaHost}`);
  }

  /**
   * Kafka Consumer 事件
   *
   * @method public
   * @param type     事件類型
   * @param callback 事件對應回乎函數
   */
  public on(
    type: 'message',
    callback?: (args: { topic: string; value: string | Buffer }) => void,
  ): void {
    if (type === 'message' && callback) {
      const outputs = OUTPUT.entries();
      this._timer = setInterval(() => {
        const output = outputs.next();
        if (!output.done) {
          callback({
            topic: this._topic.replace(/#/g, ''),
            value: JSON.stringify(output.value[1]),
          });
        } else {
          if (this._timer) {
            clearInterval(this._timer);
          }
        }
      }, 1000);
    }
  }
}
