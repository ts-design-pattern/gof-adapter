/**
 * 專案名稱： gof-adapter
 * 檔案說明： 過站時間計算服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Duration, Output } from '../models';
import { DurationBuilder } from './duration.builder';

/**
 * 過站時間計算服務
 */
export class DurationService {
  /**
   * 機台數據
   */
  private _outputs: Output[] = [];

  /**
   * 添加機台數據
   *
   * @method public
   * @param output 機台數據
   * @return 回傳物件本身
   */
  public add(output: Output): this {
    this._outputs.push(output);
    return this;
  }

  /**
   * 計算機台過站時間
   *
   * @method public
   * @return 回傳機台過站時間
   */
  public calculate(): Duration | null {
    if (this._outputs.length < 2) {
      return null;
    }

    const last = this._outputs.shift();
    const latest = this._outputs[0];
    if (last && latest) {
      return new DurationBuilder(last, latest);
    } else {
      return null;
    }
  }
}
