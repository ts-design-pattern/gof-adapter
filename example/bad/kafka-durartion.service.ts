/**
 * 專案名稱： gof-adapter
 * 檔案說明： 使用 Kafka 過站時間計算服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { ConsumerGroup, DurationService } from './shared';

/**
 * 使用 Kafka 過站時間計算服務
 */
export class KafkaDurartionService {
  /**
   * 過站時間計算服務
   */
  private _durationService = new DurationService();

  /**
   * @param _consumer Kafka Consumer Group
   */
  constructor(private _consumer: ConsumerGroup) {}

  /**
   * 執行過站時間計算
   *
   * @method public
   */
  public execute(): void {
    this._consumer.on('message', message => {
      const value = JSON.parse(message.value.toString());
      const duration = this._durationService.add(value).calculate();
      if (duration !== null) {
        console.log(
          `${duration.from} to ${duration.to} duration: ${duration.duration} by kafka`,
        );
      }
    });
  }
}
